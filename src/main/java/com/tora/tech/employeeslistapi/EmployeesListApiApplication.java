package com.tora.tech.employeeslistapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EmployeesListApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(EmployeesListApiApplication.class, args);
	}

}
